# Library Management System
A Library Management System that I made using C#, .NET and Windows Forms.
This project was made just for fun in a week of time when I learned C# so that I can make something tangible.
It implements all the checks so that there is no erros when books are added, removed, issued from the database etc.

*To make the system working, the connetion string needs to be changed so that it can connect to the right server.* 

## Screenshots of the UI

### Login Page
![Login Screenshot](./screens/7.PNG?raw=true)
### Menu for Admin
![Menu Screenshot](./screens/1.PNG?raw=true)
### User Book Search
![User Book Search Screenshot](./screens/8.PNG?raw=true)
### User Issued Books
![User Issued Books Screenshot](./screens/9.PNG?raw=true)
### Admin Issue Books
![Admin Issue Books](./screens/3.PNG?raw=true)
### Admin Return Books
![Admin Return Books](./screens/4.PNG?raw=true)
### Admin Edit Book Database
![Admin Edit Book Database](./screens/5.PNG?raw=true)
### Admin Edit User Database
![Admin Edit User Database](./screens/6.PNG?raw=true)
### Admin Book Search
![Admin Book Search Database](./screens/2.PNG?raw=true)	
