create database Library;
use Library;
go

create table books(
book_id int primary key not null,
title varchar(20) not null,
author varchar(20) not null, 
publisher varchar(20) not null,
year_of_pub numeric(4) not null,
genres varchar(20) not null,
);


create table users(
user_id int primary key not null,
admin	bit not null,
name varchar(20) not null,
address varchar(20) not null,
password varchar(20) not null,
email varchar(20) not null,
);

create table issue(
i_user_id int not null,
i_book_id int not null,
name varchar(20) null,
title varchar(20) null,
date_issued date null,
datediff date null,
constraint FK_ISSUE_BOOK foreign key(i_book_id) references books(book_id),
constraint FK_ISSUE_USER foreign key(i_user_id) references users(user_id)
);



select * from books;
select * from users;

drop table books;
drop table issue;
drop table users;