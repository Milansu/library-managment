USE [Library]
GO
INSERT [dbo].[books] ([book_id], [title], [author], [publisher], [year_of_pub], [genres]) VALUES (1, N'Godfather I', N'Fracis Ford Coppola', N'Paramount Pictures', CAST(2003 AS Numeric(4, 0)), N'drama/mystery')
GO
INSERT [dbo].[books] ([book_id], [title], [author], [publisher], [year_of_pub], [genres]) VALUES (2, N'Autobiografija', N'Milan Ciganovic', N'Knjizara Delfin', CAST(2014 AS Numeric(4, 0)), N'autobiografija')
GO
INSERT [dbo].[books] ([book_id], [title], [author], [publisher], [year_of_pub], [genres]) VALUES (3, N'Spiderman 3', N'Joe Kepler', N'Marvel', CAST(2014 AS Numeric(4, 0)), N'action')
GO
INSERT [dbo].[books] ([book_id], [title], [author], [publisher], [year_of_pub], [genres]) VALUES (4, N'Fargo', N'Noah Hawley', N'FX', CAST(2011 AS Numeric(4, 0)), N'crime')
GO
INSERT [dbo].[books] ([book_id], [title], [author], [publisher], [year_of_pub], [genres]) VALUES (5, N'Aladdin', N'Guy Ritchie', N'Pariz', CAST(2019 AS Numeric(4, 0)), N'comedy')
GO
INSERT [dbo].[users] ([user_id], [admin], [name], [address], [password], [email]) VALUES (1, 1, N'Milan', N'Sivacki put 6/A', N'admin', N'email@gmail.com')
GO
INSERT [dbo].[users] ([user_id], [admin], [name], [address], [password], [email]) VALUES (2, 1, N'Milos', N'Sivacki put 6/A', N'admin', N'email@gmail.com')
GO
INSERT [dbo].[users] ([user_id], [admin], [name], [address], [password], [email]) VALUES (3, 0, N'Branko', N'Prvomajska 2', N'password', N'branko@gmail.com')
GO
INSERT [dbo].[users] ([user_id], [admin], [name], [address], [password], [email]) VALUES (4, 0, N'Petar', N'Petrovaradinska 16', N'petar', N'petar@gmail.com')
GO
INSERT [dbo].[users] ([user_id], [admin], [name], [address], [password], [email]) VALUES (5, 0, N'Luka', N'Somborski put 16', N'luka', N'luka@gmail.com')
GO
INSERT [dbo].[issue] ([i_user_id], [i_book_id], [name], [title], [date_issued], [datediff]) VALUES (2, 3, N'Joe Kepler', N'Spiderman 3', CAST(0x022E0B00 AS Date), CAST(0x913D0B00 AS Date))
GO
INSERT [dbo].[issue] ([i_user_id], [i_book_id], [name], [title], [date_issued], [datediff]) VALUES (2, 1, N'Fracis Ford Coppola', N'Godfather I', CAST(0xBD3F0B00 AS Date), CAST(0xBD3F0B00 AS Date))
GO
